import React from 'react';
import { Router, Link } from '@reach/router';
import { Menu, Icon } from 'antd';

import Posts from './Posts';
import PostDetail from './PostDetail';
import CreatePost from './CreatePost';
import UpdatePost from './UpdatePost';

function App() {
    return (
      <div>
        <div>
          <Menu mode="horizontal">
            <Menu.Item key="posts">
              <Icon type="unordered-list" />
              <Link to="/posts">Posts</Link>
            </Menu.Item>
            <Menu.Item key="create_post">
              <Icon type="edit" />
              <Link to="/create-post">Create Post</Link>
            </Menu.Item>
          </Menu>
        </div>
        <Router>
          <Posts default />
          <PostDetail path="post/:id" />
          <CreatePost path="create-post" />
          <UpdatePost path="post/:id/edit" />
        </Router>
      </div>
    )
}

export default App;
