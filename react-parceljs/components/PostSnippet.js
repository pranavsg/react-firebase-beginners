import React from 'react';
import { Card } from 'antd';
import { Link } from '@reach/router';

const PostSnippet = props => (
  <Card title={props.title} extra={
    <div>
      <Link to={`/post/${props.id}`} style={{ marginRight: '15px' }}>READ MORE</Link>
      <Link to={`/post/${props.id}/edit`}>EDIT </Link>
    </div>
  } style={{ marginTop: 30 }}>
    <p>{props.body.substring(1, 1000)}</p>
  </Card>
);

export default PostSnippet;
