import React, { Component } from 'react';
import { navigate } from '@reach/router';
import { Form, Icon, Input, Button, Checkbox, Row, Col } from 'antd';

import PostHeader from './PostHeader';
import db from '../firebase';

class UpdatePost extends Component {
  state = {
    post: {}
  }

  componentDidMount() {
    // To disable submit button at the beginning.
    // this.props.form.validateFields();

    // fetch the post details
    const postRef = db.collection('posts')
      .doc(this.props.id);

    postRef.get()
      .then(post => {
        const { title, body } = post.data();
        this.props.form.setFieldsValue({
          title,
          body
        })
      })
      .catch(err => console.error(err));
  }

  handleSubmit = e => {
    e.preventDefault();

    // reference to post in db
    const postRef = db.collection('posts')
      .doc(this.props.id);

    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(values);
        // storing data
        postRef.update(values)
          .then(docRef => navigate('/posts'))
          .catch(err => console.error(err));
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const { TextArea } = Input;
    return (
      <div id="create_post_container">
        <div>
          <PostHeader title="Create Post" />
        </div>
        <Row>
          <Col span={12} offset={6} style={{marginTop: 30}}>
            <Form onSubmit={this.handleSubmit} className="login-form">
              <Form.Item>
                {getFieldDecorator('title', {
                  rules: [{ required: true, message: 'Please input your title!' }],
                })(
                  <Input
                    name=""
                    placeholder="Title"
                    autoFocus="on"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('body', {
                  rules: [{ required: true, message: 'Please input your Body!' }],
                })(
                  <TextArea
                    rows={4}
                    placeholder="Body"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Save
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }
};

const WrappedUpdatePost = Form.create({ name: 'create_post_form' })(UpdatePost);

export default WrappedUpdatePost;
