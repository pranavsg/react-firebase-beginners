import React from 'react';
import { PageHeader } from 'antd';

const PostHeader = props => (
  <PageHeader
    style={{
      border: '1px solid rgb(235, 237, 240)',
    }}
    title={props.title}
  />
);

export default PostHeader;
