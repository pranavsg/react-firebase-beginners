import React, { Component } from 'react';
import { Card } from 'antd';
import { Link } from '@reach/router';

import PostHeader from './PostHeader';
import db from '../firebase';

class PostDetail extends Component {
  state = {
    post: {}
  }

  componentDidMount() {
    const postRef = db.collection('posts')
      .doc(this.props.id);

    postRef.get()
      .then(post => this.setState({
        post: post.data()
      }))
      .catch(err => console.error(err));
  }

  render() {
    const { post } = this.state;
    return (
      <div>
        <div>
          <PostHeader title="Post" />
        </div>
        <Card key={post.id} title={post.title} style={{ marginTop: 30 }}>
          <p>{post.body}</p>
        </Card>
      </div>
    )
  }
}

export default PostDetail;
