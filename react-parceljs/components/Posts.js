import React, { Component } from 'react';

import PostSnippet from './PostSnippet';
import PostHeader from './PostHeader';
import db from '../firebase';

class Posts extends Component {
  state = {
    posts: []
  }

  componentDidMount() {
    let postsRef = db.collection('posts')
    postsRef.get()
      .then(posts => posts.forEach(post => {
        const { id } = post;
        const data = post.data();

        // create object with all the data
        const payload = {
          id,
          ...data
        };

        // set payload in state
        this.setState(prevState => ({
          posts: [
            ...prevState.posts,
            payload
          ]
        }))
      }))
  }

  render() {
    const { posts } = this.state;
    return (
      <div className="posts_container">
        <div>
          <PostHeader title="Posts" />
        </div>
        <div>
          {posts.map(post => <PostSnippet key={post.id} title={post.title}
            body={post.body} id={post.id} />)}
        </div>
      </div>
    )
  }
}

export default Posts;
