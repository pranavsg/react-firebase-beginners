import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';

import App from '../components/App';

// FOR HOT LOADING (parcel)
if (module.hot) {
  module.hot.accept();
}

ReactDOM.render(<App />, document.getElementById('root'));
