require("firebase/firestore");
import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyDyDEfNTJd1i-czhnSX6qaBlgk62l7PWH8",
  authDomain: "react-intro-blog-ed281.firebaseapp.com",
  databaseURL: "https://react-intro-blog-ed281.firebaseio.com",
  projectId: "react-intro-blog-ed281",
  storageBucket: "react-intro-blog-ed281.appspot.com",
  messagingSenderId: "489829098084",
  appId: "1:489829098084:web:96564ba4202665b8ac7571",
  measurementId: "G-S52T39G8VK"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

var db = firebase.firestore();

export const auth = firebase.auth();
export const firestore = firebase.firestore;
export default db;
