# Initialization
Setup React using Parcel by following [this](https://developerhandbook.com/parcel.js/build-a-react-web-app-with-parcel/).

We will be using `@reach/router` for routing in React, documentation is [here](https://reach.tech/router).

Unlike `react-router` where we have to use `Route` and specify the component to render
on that route, in `@reach/router` we can use the component itself and give it a prop
of `path` which decides where this component will be rendered.

```
import { Router } from '@reach/router';

import Posts from './Posts';
import Post from './Post';

....

    <Router>
        <Posts default />
        <Post path="post/:id" />
    </Router>
....
```

1. For specifying the root route or the default route which is `/` we can use the `default`prop on a component inside `<Router></Router>`.

2. For the `post/<id>` path where `id` is dynamic we use `path="post/:id"` which simply
renders the `Post` component on `post/id` where id is dynamic.

## Firebase Configuration
After you have created a new Firebase project, go to its settings and in bottom at **Your apps** section click on the *code* icon which will ask you for a nickname of your project, register
your web app and copy the code snippet in the following manner,

1. Copy the firebase script tag in **index.html** file,

```
    ...
    <script src="https://www.gstatic.com/firebasejs/7.8.2/firebase-app.js"></script>
    ...
```

2. Create a new file in project root folder and paste the firebase initialization code there

**firebase.js**
```
require("firebase/firestore");
import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "...",
  authDomain: "...",
  databaseURL: "...",
  projectId: "...",
  storageBucket: "...",
  messagingSenderId: "...",
  appId: "...",
  measurementId: "..."
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

var db = firebase.firestore();

export const auth = firebase.auth();
export const firestore = firebase.firestore;
export default db;
```

Now import db in **components/CreatePost.js**, parcel will handle the `firebase` installation.

## Database Operations
Official [documentation](https://firebase.google.com/docs/firestore/manage-data/add-data) for storing data in firestore.

We set a document when we have the ID of the new or existing document, that way we can
either update the document or create a new one with its ID.

```
db.collection('posts').doc('1').set({
  title: 'First Post',
  body: 'Body for post',
})
.then(() => console.log('Document successfully written'))
.catch(err => console.error('Error writing document'))
```

If the document does not exist, it will be created. If the document does exist, its contents
will be overwritten with the newly provided data.

You can even merge data into an existing document like,

```
var postRef = db.collection('posts').doc('1');

var mergePost = postRef.set({
  createdAt: new Date()
}, { merge: true });
```
